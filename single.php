<?php get_header(); ?>
<div class="page-title">
			<h2></h2>
</div>
<div class="page-wrapper">
	<?php get_sidebar( 'primary' ); ?>
	<div class="page-container2">
	<?php while(have_posts()) {
		the_post(); ?>
		<div class="posts-title-single">
			<h2><?php the_title(); ?></h2>
			<div class="publish-date">Lisatud: <?php the_time('m/j/y'); ?></div>
		</div>
			<?php the_content(); ?>


  <?php }
    echo paginate_links(); 
  ?>
</div>
  </div>

	<?php get_footer(); ?>