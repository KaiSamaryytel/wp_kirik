<title>
	<?php 
		if(pll_current_language() == 'et') {
		    echo 'Otsingu tulemused';
		} else if(pll_current_language() == 'en') {
		    echo 'Search results'; 
		} else if(pll_current_language() == 'ru') {
		    echo 'Результаты поиска'; 
		}  
    ?>
</title>
<?php get_header(); ?>
<div class="page-title">
			<h2>
				<?php 
	                if(pll_current_language() == 'et') {
	                    echo 'Otsingu tulemused';
	                } else if(pll_current_language() == 'en') {
	                    echo 'Search results'; 
	                } else if(pll_current_language() == 'ru') {
	                    echo 'Результаты поиска'; 
	                }  
            	?>
			</h2>
</div>

<div class="page-wrapper">
	<div class="page-container2">
	<?php while(have_posts()) {
		the_post(); ?>
		<div class="posts-title">
			<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		</div>
		<?php echo wp_trim_words(get_the_content(), 26); ?>
	<?php } ?>
	<hr>
	<div class="num-pagination">
    <?php echo paginate_links(); ?>	
	</div>
	</div>
</div>
	<?php get_footer(); ?>