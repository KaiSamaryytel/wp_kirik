<?php get_header(); ?>
	<div class="page-title">
			<h2><?php the_title(); ?></h2>
	</div>
<div class="page-wrapper">
	<div class="page-container">
	<?php while(have_posts()) {
		the_post(); ?>
		<div class="kuukiri-btns-container">
	<a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank" class="kuukiri-btns">Kuukiri <br> <span style="font-weight: bold;">Detsember 2018</span></a>
	<a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank" class="kuukiri-btns">Kuukiri <br> <span style="font-weight: bold;">November 2018</span></a>
	<a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank" class="kuukiri-btns">Kuukiri <br> <span style="font-weight: bold;">Oktoober 2018</span></a>
	<a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank" class="kuukiri-btns">Kuukiri <br> <span style="font-weight: bold;">September 2018</span></a>
	<a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank" class="kuukiri-btns">Kuukiri <br> <span style="font-weight: bold;">August 2018</span></a>
	<a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank" class="kuukiri-btns">Kuukiri <br> <span style="font-weight: bold;">Juuli 2018</span></a>
	<a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank" class="kuukiri-btns">Kuukiri <br> <span style="font-weight: bold;">Juuni 2018</span></a>
	<a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank" class="kuukiri-btns">Kuukiri <br> <span style="font-weight: bold;">Mai 2018</span></a>
		</div>

		<?php the_content(); ?>
	</div>
</div>
	<?php }

	get_footer(); ?>