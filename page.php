<?php get_header(); ?>
	<div class="page-title">
			<h2><?php the_title(); ?></h2>
	</div>
<div class="page-wrapper">
	<div class="page-container">
	<?php while(have_posts()) {
		the_post(); ?>
		<?php the_content(); ?>
	</div>
</div>
	<?php }

	get_footer(); ?>