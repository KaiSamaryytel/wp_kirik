<?php get_header(); ?>
<div class="index-container">
	<?php while(have_posts()) {
		the_post(); ?>
	<div class="front-text-container">
		<div class="text-background">
			<h1>Rannu kirikut näidati ETV saatesarjas “Tähendamisi”</h1>
			<p>
				Head sõbrad,
				ETV saatesarja “Tähendamisi” raames valmis ka episood Rannu kirikust.<br>

				Saade on praegu veel järelvaadatav siin:

				<a href="https://etv.err.ee/922160/tahendamisi" target="_blank">https://etv.err.ee/922160/tahendamisi</a><br>

				<br>
				
				

Anname teile teada, et alates 12. maist 2019 (kaasa arvatud) algavad pühapäevased jumalateenistused Rannu kirikus kell 10:00. Kaetud on armulaud. Täpsemat infot jumalateenistuste kohta saab meie kodulehekülje kalendrist.
<br><br>
				Õnnistussooviga,

				Timo Svedko<br>

				EELK Rannu Püha Martini koguduse õpetaja 
			</p>
		</div>
	</div>
</div>
	<?php }

	get_footer();

?>